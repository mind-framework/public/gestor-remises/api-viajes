module gitlab.com/mind-framework/public/gestor-remises/api-viajes

go 1.15

// replace gitlab.com/mind-framework/core/mind-core-api => /home/project/src/gitlab.com/mind-framework/core/mind-core-api

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/render v1.0.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/rs/zerolog v1.20.0
	gitlab.com/mind-framework/core/mind-core-api v0.1.5
)
