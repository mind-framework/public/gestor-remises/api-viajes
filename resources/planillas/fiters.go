package planillas

import (
	// "encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
)


//validFilters es una mapa de Filtros disponibles para este modelo
// los filtros son utilizados en la consulta a la BD
var validFilters map[string]model.Filter


func init() {
	log.Debug().Msg("Inicializando Model")
	validFilters = make(map[string]model.Filter)

	validFilters["idAgencia"] = model.Filter{
		Description: "Filtra por ID de Agencia",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("planillas.id_agencia", false),
	}

	validFilters["estado"] = model.Filter{
		Description: "Filtra por Estado",
		ValidValues: []string{"Abierta", "Cerrada", "Anulada", "Suspendida"},
		Parser: model.NewGenericFilterParser("planillas.estado", false),
	}

	validFilters["estadoMovil"] = model.Filter{
		Description: "Filtra por Estado del Movil",
		ValidValues: []string{"Libre", "Ocupado"},
		Parser: model.NewGenericFilterParser("estado_movil", false),
	}

	validFilters["fecha"] = model.Filter{
		Description: "Filtra por Fecha de Apertura de planilla",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("fecha_apertura", false),
	}

	validFilters["movil"] = model.Filter{
		Description: "Filtra por movil",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("movil", false),
	}


}
