package lugares

import (
	// "encoding/json"
	_log "github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
)
var log = _log.With().Str("pkg", "lugares").Logger()

//validFilters es una mapa de Filtros disponibles para este modelo
// los filtros son utilizados en la consulta a la BD
var validFilters map[string]model.Filter


func init() {
	log.Debug().Msg("Inicializando Model")
	validFilters = make(map[string]model.Filter)

	validFilters["q"] = model.Filter{
		Description: "Buscar Lugar por Nombre o Nombre alternativo",
		ValidValues: []string{".*"},
		Parser: func(values []string) (string, []string, error){
			var valores []string
			valores = append(valores, "%" + values[0] + "%")
			valores = append(valores, "%" + values[0] + "%")
			return "(nombre like ? or nombre_alternativo like ?)", valores, nil
		},
	}

	validFilters["idAgencia"] = model.Filter{
		Description: "Filtra por ID de Agencia",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("id_agencia", false),
	}

	validFilters["nombre"] = model.Filter{
		Description: "Filtra por Nombre de Lugar",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("nombre", true),
	}

	validFilters["localidad"] = model.Filter{
		Description: "Filtra por Localidad",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("localidad", true),
	}

		validFilters["tipo"] = model.Filter{
		Description: "Filtra por Tipo de Lugar (calle, lugar)",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("tipo", false),
	}


}
