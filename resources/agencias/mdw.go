package agencias
import (
	"net/http"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"strconv"
)


//FiltrarAgenciaMdw agrega filtro idAgencia para que el modelo solo devuelva los registros asociados a esta agencai
func FiltrarAgenciaMdw(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rp := middlewares.GetContextParams(r)
		log.Trace().Interface("requestParams", rp).Msg("")
		idAgencia := rp.Chi.URLParam("id_agencia")
		qp := rp.QueryParams
		log.Trace().Interface("queryParams", qp).Msg("")
		qp["idAgencia"] = []string{idAgencia}
		
		next.ServeHTTP(w, r)
	})
}

//AgregarIDAgenciaMdw inserta el campo idAgencia en el requestbody
// Sólo aplica a método POST
func AgregarIDAgenciaMdw(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if(r.Method != "POST") {
			next.ServeHTTP(w, r)
			return
		}
		rp := middlewares.GetContextParams(r)
		// log.Debug().Interface("r.Method", r.Method).Msg("AgregarIDAgenciaMdw")
		rb := rp.RequestBody.Data.([]interface{}) //.(map[string]interface{})
		idAgencia := rp.Chi.URLParam("id_agencia")
		for i := range rb {
			payload := rb[i].(map[string]interface{})
			payload["idAgencia"], _ = strconv.Atoi(idAgencia)
		}

		next.ServeHTTP(w, r)
	})
}
