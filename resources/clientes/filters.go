package clientes

import (
	// "encoding/json"
	"strings"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
)


//validFilters es una mapa de Filtros disponibles para este modelo
// los filtros son utilizados en la consulta a la BD
var validFilters map[string]model.Filter


func init() {
	log.Debug().Msg("Inicializando Model")
	validFilters = make(map[string]model.Filter)

	validFilters["q"] = model.Filter{
		Description: "Buscar Cliente por Nombre o Dirección",
		ValidValues: []string{".*"},
		Parser: func(values []string) (string, []string, error){
			var valores []string
			valores = append(valores, "%" + values[0] + "%")
			valores = append(valores, "%" + values[0] + "%")
			return "nombre like ? or direccion_calle like ?", valores, nil
		},
	}

	validFilters["idAgencia"] = model.Filter{
		Description: "Filtra por ID de Agencia",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("id_agencia", false),
	}

	validFilters["nombre"] = model.Filter{
		Description: "Filtra por Nombre de Cliente",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("nombre", true),
	}

	validFilters["calle"] = model.Filter{
		Description: "Filtra por Dirección",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("calle", false),
	}

	validFilters["numero"] = model.Filter{
		Description: "Filtra por Altura de calle",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("numero", false),
	}


	validFilters["telefono"] = model.Filter{
		Description: "Filtra por Teléfono Fijo o Móvil",
		ValidValues: []string{".*"},
		Parser: func(values []string) (string, []string, error){
			var valores []string
			v := "%" + strings.Replace(values[0], "*", "", -1) + "%"

			valores = append(valores, v)
			valores = append(valores, v)
			return "telefono like ? or movil like ?", valores, nil
		},
	}


}
