package common

//Locacion contiene los campos para establecer direcciones (calles, localidad, etc)
type Locacion struct {
	Calle  	string `json:"calle"  db:"calle"`
	Numero  	 int 	`json:"numero"  db:"numero"`
	Piso  	 *int 	`json:"piso"  db:"piso"`
	Depto  	 *string 	`json:"depto"  db:"depto"`
	Localidad  		*string 	`json:"localidad"  db:"localidad"`
	Partido  		*string 	`json:"partido"  db:"partido"`
	Provincia  		*string 	`json:"provincia"  db:"provincia"`
	Calle1  		*string 	`json:"calle1"  db:"calle1"`
	Calle2  		*string 	`json:"calle2"  db:"calle2"`
}
