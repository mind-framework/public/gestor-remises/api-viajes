package main

import (
	"fmt"
	"os"
	"errors"
	// "github.com/rs/zerolog"
	

	// paquetes requeridos para mantejo de requests
	"net/http"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"


	//paquetes requeridos para la conexión a la BD
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	//helpers para handlers y modelos
	_ "gitlab.com/mind-framework/core/mind-core-api/app"
	"gitlab.com/mind-framework/core/mind-core-api/handler"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	
	_log "github.com/rs/zerolog/log"


	//paquetes específicos del proyecto
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/agencias"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/clientes"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/moviles"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/choferes"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/viajes"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/planillas"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/comisiones"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/resources/lugares"
)
var log = _log.With().Str("pkg", "main").Logger()



func main() {

	//obtengo los parámetros desde las variables de entorno
	apiPort := os.Getenv("API_PORT")
	if apiPort == "" { 
		apiPort = "8080"
	}

	dbType := os.Getenv("DB_TYPE")
	if dbType == "" { 
		dbType = "mysql"
	}

	connStr, err := getDBConnectionString()
	if err != nil {
		log.Fatal().Err(err).Str("cat","env").Msg("Error al Cargar Datos de conexión a BD")
	}

	
	// log.Info("Conectando con la BD...")
	log.Info().Msg("Conectando con la Base de Datos")
	log.Trace().Str("connStr", connStr).Msg("Datos de Conexión:")
	db, err := sqlx.Connect(dbType, connStr)
	if err != nil {
		log.Fatal().Err(err).Str("cat","database").Msg("No se puedo conectar a la Base de Datos")
		// log.Fatalln(err)
		}	
		
		if err := db.Ping(); err != nil {
			log.Fatal().Err(err).Str("cat","database").Msg("No se puedo conectar a la Base de Datos")
		}
	
		model.Init(db)


	//Configuración de Rutas
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middlewares.ParseParamsCtx)
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
	})	
	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`pong`))
	})


	//TODO: modificar NewCRUDHandler para que reciba como parametros una funcion y una ruta

	//Ruta para acceder a los diferentes recursos de una agencia
	r.Route("/agencias/{id_agencia}", func(r chi.Router) {
		r.Use(agencias.FiltrarAgenciaMdw)
		r.Use(agencias.AgregarIDAgenciaMdw)
		r.Mount("/clientes", handler.NewCRUDHandler(clientes.NewModel).Routes())
		r.Mount("/moviles", handler.NewCRUDHandler(moviles.NewModel).Routes())
		r.Mount("/choferes", handler.NewCRUDHandler(choferes.NewModel).Routes())
		r.Mount("/viajes", handler.NewCRUDHandler(viajes.NewModel).Routes())
		r.Mount("/planillas", handler.NewCRUDHandler(planillas.NewModel).Routes())
		r.Mount("/comisiones", handler.NewCRUDHandler(comisiones.NewModel).Routes())
		r.Mount("/lugares", handler.NewCRUDHandler(lugares.NewModel).Routes())
	})

	//ruta para acceder a la información de Agencias
	//TODO: hacer funcionar metodos GET POST PATCH y DELETE sobre agencias
	r.Mount("/agencias", handler.NewCRUDHandler(agencias.NewModel).Routes())

	log.Info().Str("puerto", apiPort).Msg("Iniciando Listener")

	if err := http.ListenAndServe(":" + apiPort, r); err != nil {
		log.Fatal().Err(err).Str("cat","http").Msg("No se pudo iniciar servicio http")
	}

}


//getDBConnectionString devuelve el connection string utilizando los datos recibidos en las variables de entorno 
func getDBConnectionString() (string, error) {
	log = log.With().Str("ctx","getDBConnectionString").Logger()
	connStr := os.Getenv("DB_CONNECTION_STRING")
	if connStr == "" {
		dbHost := os.Getenv("DB_HOST")
		if dbHost == "" {
			return "", errors.New("DB_HOST no especificado")
		}
		log.Debug().Str("DB_HOST", dbHost).Msg("env")

		dbPort := os.Getenv("DB_PORT")
		if dbPort == "" {
			dbPort = "3306"
			log.Warn().Str("Puerto", dbPort).Msg("No se definió el puerto de conexión a BD, utilizando puerto por default")
		}
		log.Debug().Str("DB_PORT", dbPort).Msg("env")
	
		dbName := os.Getenv("API_DB_NAME")
		if dbName == "" {
			return "", errors.New("API_DB_NAME no especificado")
		}
		log.Debug().Str("API_DB_NAME", dbName).Msg("env")
	
	
		dbUserName := os.Getenv("API_DB_USERNAME")
		if dbUserName == "" {
			return "", errors.New("API_DB_USERNAME no especificado")
		}
		log.Debug().Str("API_DB_USERNAME", dbUserName).Msg("env")
	
		dbPassword := os.Getenv("API_DB_SECRET")
		if dbPassword == "" {
			return "", errors.New("API_DB_SECRET no especificado")
		}
		connStr = fmt.Sprintf("%s:%s@(%s:%s)/%s", dbUserName, dbPassword, dbHost, dbPort, dbName)
	}
	return connStr, nil
}
