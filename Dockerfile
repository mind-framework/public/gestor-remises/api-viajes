FROM alpine
WORKDIR /app
ADD bin/apis api
ENTRYPOINT [ "/app/api"]

